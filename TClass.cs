﻿/*----------------------------------/
/---------ToLuaPkgGenerator---------/
/--------------2017-----------------/
/----Copyright © Mathew Aloisio-----/
/----------------------------------*/

/*Free for both commercial & non commercial use.*/
/*Based off of Josh Klint's (Leadwerks CEO) BlitzMax version.*/

using System.Collections.Generic;
using System.IO;

namespace ToLuaPkgGenerator {
    public class TClass {
        // Member(s)
        public string name;
        public string prefix;
        public TClass parent;
        public string parentName;
        public List<string> members;
        public bool hasClassNamedMember; //tempfix - ToLua++ bug, see README.md

        private bool written;

        // Static Member(s)
        static public Dictionary<string, Dictionary<string, TClass>> map = new Dictionary<string, Dictionary<string, TClass>>();

        // Constructor/Destructor
        public TClass(string pName, string pNamespace) {
            name = pName;
            parent = null;
            parentName = "";
            written = false;
            hasClassNamedMember = false; //tempfix - ToLua++ bug, see README.md
            members = new List<string>();
            if (!map.ContainsKey(pNamespace))
                map.Add(pNamespace, new Dictionary<string, TClass>());
            if (!map[pNamespace].ContainsKey(pName))
                map[pNamespace].Add(pName, this);
        }

        // Methods
        public void Write(StreamWriter pStream, string padding) {
            if (written)
                return;

            TClass baseClass = parent;
            while (baseClass != null) { // Write parent classes & their parents.
                baseClass.Write(pStream, padding);
                baseClass = baseClass.parent;
            }

            if (parentName.Length != 0) { // Write class name w/ or w/o parent name.
                pStream.WriteLine(padding + prefix + name + " : public " + parentName);
            }
            else { pStream.WriteLine(padding + prefix + name); }
            pStream.WriteLine(padding + "{");
            foreach (string _member in members) {
                string member = _member;
                foreach (string _type in Program._bannedTypes.Keys) {
                    if (member.Contains(_type))
                        member = member.Replace(_type, Program._bannedTypes[_type]);
                }
                pStream.WriteLine(padding + "\t" + member);
            }
            pStream.WriteLine(padding + "};" + System.Environment.NewLine);
          
            written = true;
        }

        // Static Methods
        static public TClass Find(string pName, string pNamespace) {
            return (map.ContainsKey(pNamespace) && map[pNamespace].ContainsKey(pName)) ? 
                map[pNamespace][pName] : null;
        }
    }
}
