﻿/*----------------------------------/
/---------ToLuaPkgGenerator---------/
/--------------2017-----------------/
/----Copyright © Mathew Aloisio-----/
/----------------------------------*/

/*Free for both commercial & non commercial use.*/
/*Based off of Josh Klint's (Leadwerks CEO) BlitzMax version.*/

using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
#if DEBUG
using System.Diagnostics;
#endif

namespace ToLuaPkgGenerator {
    static public class NSMembers {
        static public Dictionary<string, List<string>> namespaces = new Dictionary<string, List<string>>();

        static private Dictionary<string, bool> written = new Dictionary<string, bool>();

        static public void AddMember(string pMember, string pNamespace) {
            if (!written.ContainsKey(pNamespace))
                written.Add(pNamespace, false);
            if (!namespaces.ContainsKey(pNamespace))
                namespaces.Add(pNamespace, new List<string>());
            namespaces[pNamespace].Add(pMember);
        }

        static public void WriteMembers(StreamWriter pStream, string pNamespace) {
            if (!written.ContainsKey(pNamespace) || written[pNamespace] == true)
                return;

            string padding = pNamespace.Length == 0 ? "" : "\t";

            foreach (string _member in namespaces[pNamespace]) {
                string member = _member;
                foreach (string _type in Program._bannedTypes.Keys) {
                    if (member.Contains(_type))
                        member = member.Replace(_type, Program._bannedTypes[_type]);
                }
                pStream.WriteLine(padding + member);
            }
       
            written[pNamespace] = true;
        }
    }

    class Program {
        public static readonly Dictionary<string, string> _bannedTypes = new Dictionary<string, string> {
            { "GetEntryUserID_", "GetEntryUserID_ @ GetEntryUserID" },
            { "GetUserID_", "GetUserID_ @ GetUserID" },
            { "size_t", "long long" },
            { "uint8_t", "unsigned char" },
            { "uint16_t", "unsigned short" },
            { "uint32_t", "unsigned int" },
            { "uint64_t", "unsigned long long" },
            { "int8_t", "char" },
            { "int16_t", "short" },
            { "int32_t", "int" },
            { "int64_t", "long long" }
        };

        private static bool wroteImport = false;

        /*private static readonly Dictionary<string, string> _bannedAssignments = new Dictionary<string, string> {
            { "size_t", "long long" },
            { "uint8_t", "unsigned char" },
            { "uint16_t", "unsigned short" },
            { "uint32_t", "unsigned int" },
            { "int8_t", "char" },
            { "int16_t", "short" },
            { "int32_t", "int" }
        };
        static void ReplaceAssignment(ref string pIn) {
            // Check for banned default assignments:
            foreach (string key in _bannedAssignments.Keys) {
                if (pIn.Contains(key) && pIn.Contains("=")) {
                    pIn = pIn.Replace(key, _bannedAssignments[key]).Trim();
                }
            }
        }

        static string RemoveDefaultsFromMethod(string pIn) {
            string pOut = pIn;
            int indexStart = pIn.IndexOf('(');
            int indexEnd = pIn.LastIndexOf(')');     
            if (indexStart == (indexEnd - 1)) // Skip (); style arguments.
                return pOut;
            if (indexStart != -1 && indexEnd != -1) {
                string[] arguments = pIn.Substring(
                    indexStart, 
                    indexEnd - indexStart).Trim().Split(',');
                for (int i = 0; i < arguments.Length; ++i) {
                    ReplaceAssignment(ref arguments[i]);
                }
                pOut = pIn.Substring(0, indexStart) +
                       string.Join(",", arguments) +
                       pIn.Substring(indexEnd);
            }
            else if ((indexStart != -1 && indexEnd == -1)) {
                Console.WriteLine("ERROR: Method declarations stretched over multiple lines are NOT supported!");
            }

            return pOut;
        }*/

        static void FindLua(string pPath) {
            if (!File.Exists(pPath)) {
                Console.WriteLine("WARNING: Failed to open non-existant file \"" + pPath + "\"");
                return;
            }
            try {
                using (StreamReader stream = new StreamReader(File.OpenRead(pPath))) {
                    string currentNamespace = "";
                    TClass cppClass = null;
                    while (stream.Peek() >= 0) {
                        string line = stream.ReadLine().Trim();
                        if (line.Length == 0) continue;
                        if (!line.Contains("using") && line.Contains("namespace")) {
                            string[] strings = line.Split(' ');
                            currentNamespace = strings[1];
                            continue;
                        }
                        string[] lineStrings = line.Split(new string[] { "//" }, StringSplitOptions.None);
                        if (lineStrings.Length != 0) {
                            string tag = lineStrings[lineStrings.Length - 1].Trim();
                            if (tag.ToLower() == "lua") {
                                line = lineStrings[0].Trim();
                                string[] strings = line.Split(' ');   
                                bool containsClass = line.Contains("class");
                                if (containsClass || line.Contains("struct")) {
                                    // Class declaration.
                                    cppClass = new TClass(strings[1], currentNamespace);        
                                    cppClass.prefix = containsClass ? "class " : "struct ";
                                    if (strings.Length > 4)
                                        cppClass.parentName = strings[4].Trim();
                                }
                                else {
                                    // Class member.
                                    string member = lineStrings[lineStrings.Length - 2].Trim();
                                    if (cppClass != null) {                                 
                                        // Remove default values from member. i.e: Method(std::string pValue = "Default!")                        
                                        //member = RemoveDefaultsFromMethod(member);

                                        // Add member to list of members.
                                        cppClass.members.Add(member);
                                    }
                                    else {
                                        NSMembers.AddMember(member, currentNamespace);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception) {
                Console.WriteLine("EXCEPTION:" + exception);
#if DEBUG
                StackTrace trace = new StackTrace(exception, true);
                Console.WriteLine("\tFILE:" + trace.GetFrame(0).GetFileName());
                Console.WriteLine("\tLINE: " + trace.GetFrame(0).GetFileLineNumber());
#endif
            }
        }

        static void ReadHeaders(string pDir) {
            string[] headers = Directory.GetFiles(pDir, "*.h", SearchOption.AllDirectories);
            foreach (string header in headers) {
                FindLua(header);
            }
        }

        static void Main(string[] pArgs) {
            string directory = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) + "/";
            Console.WriteLine("Generating pkg files...");

            // Read all header files and gather class information.
            ReadHeaders(directory);
            foreach (string argument in pArgs) {
                if (!Directory.Exists(argument)) {
                    Console.WriteLine("ERROR: Invalid directory \"" + argument + "\". Skipping directory.");
                    continue;
                }
                ReadHeaders(argument);
            }

            // Traverse over all namespaces, generate a file per namespace.
            foreach (var pair in TClass.map) {
                try {
                    // Open the luacommands.pkg file for writing.
                    string packageFilePath = pair.Key + "_pkg.pkg";
                    if (File.Exists(packageFilePath))
                        File.Delete(packageFilePath);
                    using (StreamWriter stream = new StreamWriter(File.Create(packageFilePath))) {
                        Console.WriteLine("Generating pkg file: \"{0}\"...", packageFilePath);

                        // Write include & namespace header.
                        stream.WriteLine("$/* Add includes here. */");
                        if (pair.Key == "Leadwerks") {
                            stream.WriteLine("$#include \"Leadwerks.h\"");
                        }
                        else { stream.WriteLine("$#include \"<CHANGEME>.h\""); }
                        string padding;
                        if (pair.Key.Length != 0) {
                            stream.WriteLine("$using namespace " + pair.Key + ";");
                            stream.WriteLine("\nmodule " + pair.Key + " {");
                            padding = "\t";
                        }
                        else { padding = ""; }

                        // Write namespaced (or global) members.
                        NSMembers.WriteMembers(stream, pair.Key);

                        // TClass pass #1 (Assign parents.)
                        foreach (TClass cppClass in pair.Value.Values) {
                            if (cppClass.parentName.Length != 0) {
                                cppClass.parent = TClass.Find(cppClass.parentName, pair.Key);
                            }
                        }

                        // TClass pass #2 (//tempfix - ToLua++ bug, see README.md)
                        foreach (TClass cppClass in pair.Value.Values) {
                            foreach (string member in cppClass.members) {
                                foreach (string className in pair.Value.Keys) {
                                    if (Regex.IsMatch(member, @"(^|\s)" + className + @"(\s|$)") || Regex.IsMatch(member, @"(^|\s)" + className + @";(\s|$)")) {
                                        cppClass.hasClassNamedMember = true;
                                        break;
                                    }
                                }
                                if (cppClass.hasClassNamedMember)
                                    break;
                            }
                        }

                        // TClass pass #3 (Write hasClassNamedMember classes to pkg.)
                        foreach (TClass cppClass in pair.Value.Values) {
                            if (!cppClass.hasClassNamedMember) //tempfix - ToLua++ bug, see README.md
                                continue;
                            cppClass.Write(stream, padding);
                        }

                        // TClass pass #4 (Write the rest of the pkg file.)
                        foreach (TClass cppClass in pair.Value.Values) {
                            if (cppClass.hasClassNamedMember) //tempfix - ToLua++ bug, see README.md
                                continue;
                            cppClass.Write(stream, padding);
                        }
                        if (pair.Key.Length != 0)
                            stream.WriteLine("}"); // Close module.
                        //(Leadwerks only) Write import (AFTER MODULE IS CLOSED).
                        if (!wroteImport && pair.Key == "Leadwerks") {
                            stream.WriteLine("bool import(const std::string& path);");
                            wroteImport = true;
                        }
                    }
                }
                catch (Exception exception) { Console.WriteLine(exception); }
            }

            // Write export header.
            string headerFilePath = "tolua_export.h";
            if (File.Exists(headerFilePath))
                File.Delete(headerFilePath);
            using (StreamWriter stream = new StreamWriter(File.Create(headerFilePath))) {
                stream.WriteLine("// ToLua++ Export Header");
                stream.WriteLine("// *This file was generated automatically by ToLuaPkgGenerator.*");
                stream.WriteLine("#ifndef _TOLUA_EXPORT_HEADER_");
                stream.WriteLine("#define _TOLUA_EXPORT_HEADER_");
                stream.WriteLine("#pragma once" + Environment.NewLine);
                stream.WriteLine("#include \"tolua++.h\"" + Environment.NewLine);
                foreach (string className in TClass.map.Keys) {
                    stream.WriteLine("TOLUA_API int  tolua_" + className + "_pkg_open (lua_State* tolua_S);");
                }
                stream.WriteLine("#endif");
            }

            Console.WriteLine("Finished generating pkg & header files!");
        }
    }
}